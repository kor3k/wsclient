<?php

//declare(strict_types=1);

namespace App\Command;

use App\WebSockets\BittrexBot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Process\Process;

use App\WebSockets\BittrexClient as Client;

class WatchdogCommand extends ContainerAwareCommand
{
    /** @var \Psr\Log\LoggerInterface */
    private $logger;

    protected function configure()
    {
        $this
            ->setName('app:bittrex')
            ->setDescription('bittrex price change watchdog')
            ->setHelp('no help')
            ->addOption(
                'frequency',
                'f',
                InputOption::VALUE_OPTIONAL,
                'frequency in seconds',
                '30'
            )
            ->addOption(
                'delta',
                'd',
                InputOption::VALUE_OPTIONAL,
                'delta in percents',
                '5'
            )
            ->addOption(
                'currency',
                'c',
                InputOption::VALUE_OPTIONAL,
                'currency pair code',
                'USD-BTC'
            )
        ;
    }

    protected function initialize( InputInterface $input , OutputInterface $output )
    {
        $this->logger   =   $this->getContainer()->get('logger');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currency   =   $input->getOption('currency');
        $delta      =   $input->getOption('delta');
        $frequency  =   $input->getOption('frequency');

//        $this->logger->alert('test');
        $this->logger->info( '<question>starting bittrex price change watchdog</question>' ,
            [
                '<comment>currency</comment>'  =>  "<info>{$currency}</info>" ,
                '<comment>delta</comment>'     =>  "<info>{$delta}</info>"  ,
                '<comment>frequency</comment>' =>  "<info>{$frequency}</info>"  ,
            ]);

//        $this->runClient();
        $this->runBot($currency, $delta, $frequency);
    }

    protected function runBot( string $currency , float $delta , int $frequency )
    {
        $client =   new Client( $this->logger );
        $bot    =   new BittrexBot(
            $this->logger , $client ,
            $currency, $delta, $frequency
        );

        if( 'true' === $this->getContainer()->getParameter('flush_spool') )
        {
            $bot->setCallback(function($context)
            {
                $cmd    =   sprintf(
                    'php bin/console swiftmailer:spool:send --env=%s' ,
                    $this->getContainer()->getParameter('kernel.environment')
                );
                $this->logger->info('flushing spool');
                $proc   =   new Process( $cmd , __DIR__.'/../../' );
                $proc->start();
            });
        }

        $bot->run();
    }

    protected function runClient()
    {
        $client =   new Client( $this->logger );

        $client->subscribeToSummaryLiteDeltas( function($data)
        {
            $this->logger->info( 'received data' , [$data] );
        } );

        $client->run();
    }
}
