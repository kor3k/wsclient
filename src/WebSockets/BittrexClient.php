<?php

//declare(strict_types=1);

namespace App\WebSockets;

use Psr\Log\LoggerInterface;

class BittrexClient extends SignalrClient
{
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger, 'wss://socket.bittrex.com/signalr', [ 'c2' ]);
    }

    public function subscribeToSummaryLiteDeltas(callable $subscriber)
    {
        $this->subscribe('uL', 'SubscribeToSummaryLiteDeltas' , [] , $subscriber);
    }

    public function subscribeToSummaryDeltas(callable $subscriber)
    {
        $this->subscribe('uS', 'SubscribeToSummaryDeltas' , [] , $subscriber);
    }

    protected function decodePayload( string $payload )
    {
        //TODO: is bittrex specific or not?
        $payload =   base64_decode($payload);
        $payload =   gzinflate($payload);
        $payload =   json_decode($payload);

        return $payload;
    }
}
