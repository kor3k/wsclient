<?php

//declare(strict_types=1);

namespace App\WebSockets;

use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use Ratchet\Client\Connector;
use Ratchet\RFC6455\Messaging\MessageInterface;
use React\EventLoop\Factory as LoopFactory;

class SignalrClient
{
    private $base_url;
    private $hubs;
    private $connectionToken;
    private $connectionId;
    private $loop;
    private $callbacks;
    private $messageId = 1000;
    protected $logger;

    /**
     * SignalrClient constructor.
     * @param LoggerInterface $logger
     * @param string $base_url
     * @param array $hubs
     */
    public function __construct(LoggerInterface $logger, string $base_url, array $hubs = [])
    {
        $this->base_url     =   $base_url;
        $this->hubs         =   $hubs;
        $this->logger       =   $logger;
        $this->callbacks    =   [];
    }

    public function run()
    {
        $this->logger->debug( 'negotiating...' );
        if(!$this->negotiate()) {
            throw new \RuntimeException("Cannot negotiate");
        }

        $this->logger->debug( 'connecting...' );
        $this->connect();

        if(!$this->start()) {
            throw new \RuntimeException("Cannot start");
        }

        $this->logger->debug( 'running...' );
        $this->logger->info( 'connected and running...' );
        $this->loop->run();
    }

    public function subscribe(
        string $channel,
        string $method,
        array $arguments,
        callable $subscriber,
        array $hubs = null
    ) : void
    {
        if( null === $hubs )
        {
            $hubs   =   $this->hubs;
        }

        if(empty($this->hubs))
        {
            throw new \RuntimeException('no hubs specified');
        }

        foreach( $hubs as $hub )
        {
            $hub    =   strtolower($hub);
            $this->callbacks[$channel]['clbks'][$hub]  =   $subscriber;
        }

        $this->callbacks[$channel]['args']  =   $arguments;
        $this->callbacks[$channel]['mthd']  =   $method;
    }

    protected function subscribeCallbacks(WebSocket $conn) : void
    {
        foreach($this->callbacks as $channel => $callbacks)
        {
            foreach($callbacks['clbks'] as $hub => $callback)
            {
                $subscribeMsg = json_encode([
                    'H' => $hub,
                    'M' => $callbacks['mthd'],
                    'A' => $callbacks['args'],
                    'I' => $this->messageId
                ]);

                $this->logger->debug( 'subscribing' , [ 'message' => $subscribeMsg , 'channel' => $channel , 'hub' => $hub ] );

                $conn->send($subscribeMsg);
            }
        }
    }

    protected function decodePayload( string $payload )
    {
        return $payload;
    }

    protected function connect() : void
    {
        $this->loop = LoopFactory::create();
        $connector = new Connector($this->loop);

        $connector($this->buildConnectUrl())->then(function(WebSocket $conn)
        {
            $this->subscribeCallbacks($conn);

            $conn->on('message', function(MessageInterface $msg) use ($conn)
            {
                $data = json_decode($msg);
//                $this->logger->debug( 'data received' , [ 'data' => $data ] );

                if(\property_exists($data, "R"))
                {
                    @$this->logger->debug( 'data received' , [ 'data' => $this->decodePayload($data->R) ] );
                }

                if(\property_exists($data, "M"))
                {
//                    $this->logger->debug( 'data received' , [ 'data' => $data ] );

                    foreach($data->M as $message)
                    {
                        $hub        = strtolower($message->H);
                        $channel    = $message->M;

                        foreach( $message->A as $payload )
                        {
                            $this->logger->debug( 'incoming' , [ 'hub' => $hub , 'channel' => $channel ] );

                            $payload    =   $this->decodePayload($payload);
//                            $this->logger->debug( 'data received' , [ 'data' => $payload ] );


                            if( isset($this->callbacks[$channel])
                                && isset($this->callbacks[$channel]['clbks'][$hub]) )
                            {
                                $callback   =   $this->callbacks[$channel]['clbks'][$hub];

                                $this->logger->debug( 'calling event subscriber' , [ 'callback' => $callback , 'hub' => $hub , 'channel' => $channel ] );

                                $callback($payload);
                            }
                        }
                    }
                }
            });
        },
        function(\Exception $e)
        {
            echo "Could not connect: {$e->getMessage()}\n";
            $this->loop->stop();
            throw new \RuntimeException("Could not connect: {$e->getMessage()}\n");
        });
    }

    protected function buildNegotiateUrl()
    {
        $base = str_replace("wss://", "https://", $this->base_url);

        $hubs = [];
        foreach($this->hubs as $hubName) {
            $hubs[] = (object)["name" => $hubName];
        }

        $query = [
            "clientProtocol" => 1.5,
            "connectionData" => json_encode($hubs)
        ];

        return $base . "/negotiate?" . http_build_query($query);
    }

    protected function buildStartUrl()
    {
        $base = str_replace("wss://", "https://", $this->base_url);

        $hubs = [];
        foreach($this->hubs as $hubName) {
            $hubs[] = (object)["name" => $hubName];
        }

        $query = [
            "transport" => "webSockets",
            "clientProtocol" => 1.5,
            "connectionToken" => $this->connectionToken,
            "connectionData" => json_encode($hubs)
        ];

        return $base . "/start?" . http_build_query($query);
    }

    protected function buildConnectUrl()
    {
        $hubs = [];
        foreach($this->hubs as $hubName) {
            $hubs[] = (object)["name" => $hubName];
        }

        $query = [
            "transport" => "webSockets",
            "clientProtocol" => 1.5,
            "connectionToken" => $this->connectionToken,
            "connectionData" => json_encode($hubs)
        ];

        return $this->base_url . "/connect?" . http_build_query($query);
    }

    protected function negotiate()
    {
        try {
            $url = $this->buildNegotiateUrl();
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url);

            $body = json_decode((string)$res->getBody());

            $this->connectionToken = $body->ConnectionToken;
            $this->connectionId = $body->ConnectionId;
            return true;

        } catch(\Exception $e) {
            return false;
        }
    }

    protected function start()
    {
        try {
            $url = $this->buildStartUrl();
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url);

            $body = json_decode((string)$res->getBody());

            return true;

        } catch(\Exception $e) {
            return false;
        }
    }
}