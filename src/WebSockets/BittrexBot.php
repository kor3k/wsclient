<?php

//declare(strict_types=1);

namespace App\WebSockets;

use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class BittrexBot
{
    /**
     * @var BittrexClient
     */
    private $client;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var float
     */
    private $last;
    /**
     * @var float
     */
    private $delta;
    /**
     * @var string
     */
    private $currencyPair;
    /**
     * @var int
     */
    private $frequency;
    /**
     * @var callable
     */
    private $callback;

    /**
     * BittrexBot constructor.
     * @param LoggerInterface $logger
     * @param BittrexClient $client
     * @param string $currencyPair
     * @param float $delta
     * @param int $frequency
     */
    public function __construct(
        LoggerInterface $logger ,
        BittrexClient $client ,
        string $currencyPair = 'USD-BTC' ,
        float $delta = 5 ,
        int $frequency = 30 )
    {
        $this->logger   =   $logger;
        $this->client   =   $client;

        $this->delta        =   $delta;
        $this->currencyPair =   $currencyPair;
        $this->frequency    =   $frequency;
    }

    public function setCallback(callable $callback)
    {
        $this->callback =   $callback;
    }

    protected function process($data)
    {
        if( !\property_exists($data , 'D') )
        {
            return;
        }

        $last   =   $this->last;

        foreach( $data->D as $bid )
        {
            if( $this->currencyPair != $bid->M )
            {
                continue;
            }

            $curr  =   $bid->l;

            $this->logger->info( '<comment>check</comment>' , [ 'current' => $curr , 'last' => $last ] );

            if( $last && $last != $curr )
            {
                $deltaAbs   =   abs( $last - $curr );
                $delta      =   $deltaAbs / ( min( $last , $curr ) / 100 );

                $context    =   [
                    'current'   => $curr ,
                    'last'      => $last ,
                    'delta'     => $delta ,
                    'deltaAbs'  => $deltaAbs
                ];

                $this->logger->debug( '<error>before delta checking</error>' , $context );

                if( $delta >= $this->delta )
                {
                    $this->logger->alert( '<error>changed</error>' , $context );

                    if( $this->callback )
                    {
                        $clbk   =   $this->callback;
                        $clbk($context);
                    }
                }
            }

            $this->last =   $curr;
        }
    }

    public function run()
    {
        $sw =   new Stopwatch();

        $this->client->subscribeToSummaryDeltas(function($data) use($sw)
        {
//            $this->logger->info( 'bot received data' , [$data] );

            if( !$sw->isStarted('loop') )
            {
                $sw->start('loop');
                $this->process($data);
            }
            else
            {
                $elapsed    =   $sw->lap('loop')->getDuration() / 1000;

                if( $elapsed >= $this->frequency )
                {
                    $sw->stop('loop');
                    $sw->reset();
                    $this->process($data);
                    $sw->start('loop');
                }
                else
                {
                    //noop
                }
            }

            $this->logger->debug( 'bot received data' , [ 'elapsed' => $sw->lap('loop')->getDuration()/1000] );
        });


        $this->client->run();
    }
}
